import { HttpStatus, UserRole } from "../../constant.js";
import ErrorResponse from "../../utils/ErrorResponse.js";
import SuccessResponse from "../../utils/SuccessResponse.js";
import * as meetingHouseholdService from "./services/meetingHousehold.service.js";

const getMeetingHouseholdList = async (req, res) => {
   try {
      const meetingHouseholdList =
         await meetingHouseholdService.getMeetingHouseholdList();
      return res
         .status(HttpStatus.OK)
         .json(new SuccessResponse(meetingHouseholdList));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const createMeetingHousehold = async (req, res) => {
   try {
      const newMeetingHousehold =
         await meetingHouseholdService.createMeetingHousehold(req.body);
      return res
         .status(HttpStatus.OK)
         .json(new SuccessResponse(newMeetingHousehold));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const getMeetingHouseholdById = async (req, res) => {
   try {
      const meetingHousehold =
         await meetingHouseholdService.getMeetingHouseholdById(req.params.id);
      return res
         .status(HttpStatus.OK)
         .json(new SuccessResponse(meetingHousehold));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
// const getMeetingHouseholdByHouseholdId = async (req, res) => {
//    try {
//       // lay danh sach id theo householdId tu model MeetingHousehold_Household
//       let listId = [];
//       const meetingHouseholdList = await meetingHouseholdService.getMeetingHouseholdByListId(listId);
//       return res.status(HttpStatus.OK).json(new SuccessResponse(meetingHouseholdList));
//    } catch (error) {
//       return res
//          .status(HttpStatus.INTERNAL_SERVER_ERROR)
//          .json(
//             new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
//          );
//    }
// };
const deleteMeetingHousehold = async (req, res) => {
   try {
      const isDeleted = await meetingHouseholdService.getMeetingHouseholdById(
         req.params.id
      );
      if (isDeleted) {
         return res
            .status(HttpStatus.OK)
            .json(new SuccessResponse(req.params.id));
      }
      return res
         .status(HttpStatus.BAD_REQUEST)
         .json(new ErrorResponse(HttpStatus.BAD_REQUEST, "delete failed"));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
export {
   getMeetingHouseholdList,
   createMeetingHousehold,
   getMeetingHouseholdById,
   // getMeetingHouseholdByHouseholdId,
   deleteMeetingHousehold,
};
