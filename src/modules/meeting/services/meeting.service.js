import Sequelize from "sequelize";
const Op = Sequelize.Op;
import db from "../../../../models/index.cjs";
let getMeetingList = async () => {
   try {
      return await db.Meeting.findAll();
   } catch (error) {
      throw error;
   }
};
let getMeetingById = async (id) => {
   try {
      return await db.Meeting.findOne({
         where: {
            id,
         },
      });
   } catch (error) {
      throw error;
   }
};
let getMeetingByListId = async (listId) => {
   try {
      return await db.Meeting.findAll({
         where: {
            id: {
               [Op.in]: [...listId],
            },
         },
      });
   } catch (error) {
      throw error;
   }
};
let createMeeting = async (createBody) => {
   try {
      return await db.Meeting.create({ ...createBody });
   } catch (error) {
      throw error;
   }
};
let updateMeeting = async (id, updateBody) => {
   try {
      return await db.Meeting.update(
         {
            ...updateBody,
         },
         {
            where: { id },
         }
      );
   } catch (error) {
      throw error;
   }
};
let deleteMeeting = async (id) => {
   try {
      return await db.Meeting.destroy({ where: id });
   } catch (error) {
      throw error;
   }
};
export {
   getMeetingList,
   getMeetingById,
   getMeetingByListId,
   createMeeting,
   updateMeeting,
   deleteMeeting,
};
