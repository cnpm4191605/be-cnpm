import Joi from "joi";

const createTemporaryAbsent = {
   body: Joi.object({
      idNhanKhau: Joi.number().positive().required(),
      maGiayTamVang: Joi.string().trim().allow("").optional(),
      noiTamtru: Joi.string().trim().allow("").optional(),
      tuNgay: Joi.string().trim().allow("").optional(),
      denNgay: Joi.string().trim().allow("").optional(),
      lyDo: Joi.string().trim().allow("").optional(),
   }),
};
const getTemporaryAbsentById = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const getTemporaryAbsentByResidentId = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const deleteTemporaryAbsent = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
export {
   createTemporaryAbsent,
   getTemporaryAbsentById,
   getTemporaryAbsentByResidentId,
   deleteTemporaryAbsent,
};
