import { Router } from "express";
import { HttpStatus, UserRole } from "../../constant.js";
import validate from "../../middlewares/validate.js";
import * as meetingHouseholdValidator from "./meetingHousehold.validator.js";
import * as meetingHouseholdController from "./meetingHousehold.controller.js";
import { verifyToken, checkUserRole } from "../../middlewares/authJwt.js";
const router = Router();
// router.get(
//    "/household/:id",
//    [
//       verifyToken,
//       checkUserRole([
//          UserRole.PRESIDENT,
//          UserRole.VICE_PRESIDENT,
//          UserRole.SECRETARY,
//       ]),
//    ],
//    validate(meetingHouseholdValidator.getMeetingHouseholdByHouseholdId),
//    meetingHouseholdController.getMeetingHouseholdByHouseholdId
// );
// router.get(
//    "/:id",
//    [
//       verifyToken,
//       checkUserRole([
//          UserRole.PRESIDENT,
//          UserRole.VICE_PRESIDENT,
//          UserRole.SECRETARY,
//       ]),
//    ],
//    validate(meetingHouseholdValidator.getMeetingHouseholdById),
//    meetingHouseholdController.getMeetingHouseholdById
// );

// router.get(
//    "/",
//    [
//       verifyToken,
//       checkUserRole([
//          UserRole.PRESIDENT,
//          UserRole.VICE_PRESIDENT,
//          UserRole.SECRETARY,
//       ]),
//    ],

//    meetingHouseholdController.getMeetingHouseholdList
// );
router.post(
   "/",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(meetingHouseholdValidator.createMeetingHousehold),
   meetingHouseholdController.createMeetingHousehold
);
router.delete(
   "/:id",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(meetingHouseholdValidator.deleteMeetingHousehold),
   meetingHouseholdController.deleteMeetingHousehold
);
export default router;
