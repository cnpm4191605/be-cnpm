import db from "../../../../models/index.cjs";
let createMemberHousehold = async (createBody) => {
   try {
      return await db.MemberOfHousehold.create({ ...createBody });
   } catch (error) {
      throw error;
   }
};
let getMemberHouseholdById = async (id) => {
   try {
      return await db.MemberOfHousehold.findOne({
         where: {
            id,
         },
         attributes: ["id", "idHoKhau", "idNhanKhau", "quanHeVoiChuHo"],
         include: [
            {
               model: db.Resident,
               attributes: { exclude: ["id"] },
            },
         ],
      });
   } catch (error) {
      throw error;
   }
};
let getMemberHouseholdByResidentId = async (residentId) => {
   try {
      return await db.MemberOfHousehold.findOne({
         where: {
            idNhanKhau: residentId,
         },
         include: [
            {
               model: db.Household,
               attributes: ["maHoKhau", "idChuHo", "maKhuVuc", "diaChi"],
               include: [
                  {
                     model: db.Resident,
                     attributes: ["soCMT", "hoTen", "gioiTinh", "namSinh"],
                     as: "householder",
                  },
               ],
            },
         ],
      });
   } catch (error) {
      throw error;
   }
};
let getMemberHouseholdByHouseholdId = async (householdId) => {
   try {
      return await db.MemberOfHousehold.findAll({
         where: {
            idHoKhau: householdId,
         },
         include: [
            {
               model: db.Resident,
               // attributes: ["soCMT", "hoTen", "gioiTinh", "namSinh"],
            },
         ],
      });
   } catch (error) {
      throw error;
   }
};
let updateMemberHousehold = async (id, updateBody) => {
   try {
      return await db.MemberOfHousehold.update(
         {
            ...updateBody,
         },
         {
            where: { id },
         }
      );
   } catch (error) {
      throw error;
   }
};
let deleteMemberHousehold = async (id) => {
   try {
      return await db.MemberOfHousehold.destroy({ where: { id } });
   } catch (error) {
      throw error;
   }
};
let deleteMemberHouseholdByResidentId = async (residentId) => {
   try {
      return await db.MemberOfHousehold.destroy({
         where: { idNhanKhau: residentId },
      });
   } catch (error) {
      throw error;
   }
};
export {
   createMemberHousehold,
   getMemberHouseholdById,
   getMemberHouseholdByResidentId,
   getMemberHouseholdByHouseholdId,
   updateMemberHousehold,
   deleteMemberHousehold,
   deleteMemberHouseholdByResidentId,
};
