import Joi from "joi";

const createMeetingHousehold = {
   body: Joi.object({
      meetingId: Joi.number().positive().required(),
      householdId: Joi.number().positive().required(),
   }),
};
const getMeetingHouseholdById = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const getMeetingHouseholdByHouseholdId = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const deleteMeetingHousehold = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
export {
   createMeetingHousehold,
   getMeetingHouseholdById,
   getMeetingHouseholdByHouseholdId,
   deleteMeetingHousehold,
};
