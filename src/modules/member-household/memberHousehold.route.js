import { Router } from "express";
import { HttpStatus, UserRole } from "../../constant.js";
import validate from "../../middlewares/validate.js";
import * as memberHouseholdValidator from "./memberHousehold.validator.js";
import * as memberHouseholdController from "./memberHousehold.controller.js";
import { verifyToken, checkUserRole } from "../../middlewares/authJwt.js";
const router = Router();
router.get(
   "/household/:householdId",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(memberHouseholdValidator.getMemberHouseholdByHouseholdId),
   memberHouseholdController.getMemberHouseholdByHouseholdId
);
router.get(
   "/:id",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(memberHouseholdValidator.getMemberHouseholdById),
   memberHouseholdController.getMemberHouseholdById
);
router.post(
   "/",
   [verifyToken, checkUserRole([UserRole.PRESIDENT, UserRole.VICE_PRESIDENT])],
   validate(memberHouseholdValidator.createMemberHousehold),
   memberHouseholdController.createMemberHousehold
);
router.put(
   "/:id",
   [verifyToken, checkUserRole([UserRole.PRESIDENT, UserRole.VICE_PRESIDENT])],
   validate(memberHouseholdValidator.updateMemberHouseholdById),
   memberHouseholdController.updateMemberHouseholdById
);
router.delete(
   "/:id",
   [verifyToken, checkUserRole([UserRole.PRESIDENT, UserRole.VICE_PRESIDENT])],
   validate(memberHouseholdValidator.deleteMemberHousehold),
   memberHouseholdController.deleteMemberHousehold
);
router.delete(
   "/resident/:id",
   [verifyToken, checkUserRole([UserRole.PRESIDENT, UserRole.VICE_PRESIDENT])],
   validate(memberHouseholdValidator.deleteMemberHousehold),
   memberHouseholdController.deleteMemberHouseholdByResidentId
);
export default router;
