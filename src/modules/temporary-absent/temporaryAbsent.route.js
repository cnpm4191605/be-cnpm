import { Router } from "express";
import { HttpStatus, UserRole } from "../../constant.js";
import validate from "../../middlewares/validate.js";
import * as temporaryAbsentValidator from "./temporaryAbsent.validator.js";
import * as temporaryAbsentController from "./temporaryAbsent.controller.js";
import { verifyToken, checkUserRole } from "../../middlewares/authJwt.js";
const router = Router();
router.get(
   "/count",
   [verifyToken],
   temporaryAbsentController.countTemporaryAbsent
);
router.get(
   "/resident/:id",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(temporaryAbsentValidator.getTemporaryAbsentByResidentId),
   temporaryAbsentController.getTemporaryAbsentByResidentId
);
router.get(
   "/:id",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(temporaryAbsentValidator.getTemporaryAbsentById),
   temporaryAbsentController.getTemporaryAbsentById
);
router.get(
   "/",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   temporaryAbsentController.getTemporaryAbsentList
);
router.post(
   "/",
   [verifyToken, checkUserRole([UserRole.PRESIDENT, UserRole.VICE_PRESIDENT])],
   validate(temporaryAbsentValidator.createTemporaryAbsent),
   temporaryAbsentController.createTemporaryAbsent
);
router.delete(
   "/:id",
   [verifyToken, checkUserRole([UserRole.PRESIDENT, UserRole.VICE_PRESIDENT])],
   validate(temporaryAbsentValidator.deleteTemporaryAbsent),
   temporaryAbsentController.deleteTemporaryAbsent
);
export default router;
