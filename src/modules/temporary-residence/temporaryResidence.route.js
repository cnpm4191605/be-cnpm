import { Router } from "express";
import { HttpStatus, UserRole } from "../../constant.js";
import validate from "../../middlewares/validate.js";
import * as temporaryResidenceValidator from "./temporaryResidence.validator.js";
import * as temporaryResidenceController from "./temporaryResidence.controller.js";
import { verifyToken, checkUserRole } from "../../middlewares/authJwt.js";
const router = Router();
router.get(
   "/count",
   [verifyToken],
   temporaryResidenceController.countTemporaryResidence
);
router.get(
   "/resident/:id",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(temporaryResidenceValidator.getTemporaryResidenceByResidentId),
   temporaryResidenceController.getTemporaryResidenceByResidentId
);
router.get(
   "/:id",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(temporaryResidenceValidator.getTemporaryResidenceById),
   temporaryResidenceController.getTemporaryResidenceById
);

router.get(
   "/",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],

   temporaryResidenceController.getTemporaryResidenceList
);
router.post(
   "/",
   [verifyToken, checkUserRole([UserRole.PRESIDENT, UserRole.VICE_PRESIDENT])],
   validate(temporaryResidenceValidator.createTemporaryResidence),
   temporaryResidenceController.createTemporaryResidence
);
router.delete(
   "/:id",
   [verifyToken, checkUserRole([UserRole.PRESIDENT, UserRole.VICE_PRESIDENT])],
   validate(temporaryResidenceValidator.deleteTemporaryResidence),
   temporaryResidenceController.deleteTemporaryResidence
);
export default router;
