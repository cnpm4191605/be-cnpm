import { HttpStatus, UserRole } from "../../constant.js";
import ErrorResponse from "../../utils/ErrorResponse.js";
import SuccessResponse from "../../utils/SuccessResponse.js";
import * as residentService from "./services/resident.service.js";
import * as temporaryResidenceService from "../temporary-residence/services/temporaryResidence.service.js";
import * as temporaryAbsentService from "../temporary-absent/services/temporaryAbsent.service.js";
const getList = async (req, res) => {
   try {
      const { status } = req.query;
      let residents = [];
      let listId = [];
      if (status === "1") {
         // tam tru
         listId =
            await temporaryResidenceService.getTemporaryResidenceListNow();
         listId = listId.map((item) => item.idNhanKhau);
         console.log(listId);
         residents = await residentService.getList(req.query, listId);
      } else if (status === "2") {
         // tam vang
         listId = await temporaryAbsentService.getTemporaryAbsentListNow();
         listId = listId.map((item) => item.idNhanKhau);
         console.log(listId);

         residents = await residentService.getList(req.query, listId);
      } else {
         residents = await residentService.getList(req.query);
      }

      return res.status(HttpStatus.OK).json(new SuccessResponse(residents));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const getResidentListByKeyword = async (req, res) => {
   try {
      const residents = await residentService.getResidentListByKeyword(
         req.query
      );
      return res.status(HttpStatus.OK).json(new SuccessResponse(residents));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const createResident = async (req, res) => {
   try {
      let resident = await residentService.createResident({
         ...req.body,
         idNguoiTao: req.userId,
         ngayTao: new Date().toString(),
      });
      return res.status(HttpStatus.OK).json(new SuccessResponse(resident));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const deleteResidentByID = async (req, res) => {
   try {
      let resident = await residentService.updateResidentByID(
         req.params.residentID,
         {
            idNguoiXoa: req.userId,
            ngayXoa: new Date().toString(),
         }
      );
      if (resident) {
         let rowAffected = await residentService.deleteResidentByID(
            req.params.residentID
         );
         if (rowAffected > 0)
            return res
               .status(HttpStatus.OK)
               .json(new SuccessResponse(req.params.residentID));
         else
            return res
               .status(HttpStatus.NOT_FOUND)
               .json(new ErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
      }
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const getResidentByID = async (req, res) => {
   try {
      const resident = await residentService.getResidentByID(
         req.params.residentID
      );
      if (resident)
         return res.status(HttpStatus.OK).json(new SuccessResponse(resident));
      else
         return res
            .status(HttpStatus.ITEM_NOT_FOUND)
            .json(new ErrorResponse(HttpStatus.ITEM_NOT_FOUND, "Not found"));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const updateResidentByID = async (req, res) => {
   try {
      // kiểm tra xem tồn tại resident đó không
      let resident = await residentService.getResidentByID(
         req.params.residentID
      );
      if (!resident) {
         return res
            .status(HttpStatus.NOT_FOUND)
            .json(new ErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
      }
      let rowAffected = await residentService.updateResidentByID(
         req.params.residentID,
         req.body
      );
      if (rowAffected > 0) {
         let residentUpdate = await residentService.getResidentByID(
            req.params.residentID
         );
         return res
            .status(HttpStatus.OK)
            .json(new SuccessResponse(residentUpdate));
      } else {
         return res
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .json(
               new ErrorResponse(
                  HttpStatus.INTERNAL_SERVER_ERROR,
                  "upđate failed"
               )
            );
      }
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const countResident = async (req, res) => {
   try {
      const count = await residentService.countResident();

      return res.status(HttpStatus.OK).json(new SuccessResponse(count));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
export {
   createResident,
   countResident,
   deleteResidentByID,
   getResidentByID,
   updateResidentByID,
   getList,
   getResidentListByKeyword,
};
