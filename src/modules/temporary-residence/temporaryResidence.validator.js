import Joi from "joi";

const createTemporaryResidence = {
   body: Joi.object({
      idNhanKhau: Joi.number().positive().required(),
      maGiayTamtru: Joi.string().trim().allow("").optional(),
      soDienThoaiNguoiDangKy: Joi.string().trim().allow("").optional(),
      tuNgay: Joi.string().trim().allow("").optional(),
      denNgay: Joi.string().trim().allow("").optional(),
      lyDo: Joi.string().trim().allow("").optional(),
   }),
};
const getTemporaryResidenceById = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const getTemporaryResidenceByResidentId = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const deleteTemporaryResidence = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
export {
   createTemporaryResidence,
   getTemporaryResidenceById,
   getTemporaryResidenceByResidentId,
   deleteTemporaryResidence,
};
