"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class Meeting extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         Meeting.hasMany(models.Meeting_Household, {
            foreignKey: "meetingId",
            sourceKey: "id",
         });
      }
   }
   Meeting.init(
      {
         title: { type: DataTypes.STRING },
         location: { type: DataTypes.STRING },
         startTime: { type: DataTypes.DATE },
         notes: { type: DataTypes.STRING },
      },
      {
         sequelize,
         freezeTableName: true,
         paranoid: true,
      }
   );
   return Meeting;
};
