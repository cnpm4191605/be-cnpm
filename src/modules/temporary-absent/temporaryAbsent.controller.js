import { HttpStatus, UserRole } from "../../constant.js";
import ErrorResponse from "../../utils/ErrorResponse.js";
import SuccessResponse from "../../utils/SuccessResponse.js";
import * as temporaryAbsentService from "./services/temporaryAbsent.service.js";

const getTemporaryAbsentList = async (req, res) => {
   try {
      const temporaryAbsentList =
         await temporaryAbsentService.getTemporaryAbsentList();
      return res
         .status(HttpStatus.OK)
         .json(new SuccessResponse(temporaryAbsentList));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const createTemporaryAbsent = async (req, res) => {
   try {
      const newRow = await temporaryAbsentService.createTemporaryAbsent(
         req.body
      );
      if (newRow) {
         let newTemporaryAbsent =
            await temporaryAbsentService.getTemporaryAbsentById(newRow.id);
         return res
            .status(HttpStatus.OK)
            .json(new SuccessResponse(newTemporaryAbsent));
      }
      return res
         .status(HttpStatus.BAD_REQUEST)
         .json(new ErrorResponse(HttpStatus.BAD_REQUEST, "create failed"));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const getTemporaryAbsentById = async (req, res) => {
   try {
      const temporaryAbsent =
         await temporaryAbsentService.getTemporaryAbsentById(req.params.id);
      return res
         .status(HttpStatus.OK)
         .json(new SuccessResponse(temporaryAbsent));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const getTemporaryAbsentByResidentId = async (req, res) => {
   try {
      const temporaryAbsentList =
         await temporaryAbsentService.getTemporaryAbsentByResidenceId(
            req.params.id
         );
      return res
         .status(HttpStatus.OK)
         .json(new SuccessResponse(temporaryAbsentList));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const deleteTemporaryAbsent = async (req, res) => {
   try {
      const isDeleted = await temporaryAbsentService.getTemporaryAbsentById(
         req.params.id
      );
      if (isDeleted) {
         return res
            .status(HttpStatus.OK)
            .json(new SuccessResponse(req.params.id));
      }
      return res
         .status(HttpStatus.BAD_REQUEST)
         .json(new ErrorResponse(HttpStatus.BAD_REQUEST, "delete failed"));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const countTemporaryAbsent = async (req, res) => {
   try {
      const count = await temporaryAbsentService.countTemporaryAbsent();
      return res.status(HttpStatus.OK).json(new SuccessResponse(count));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
export {
   countTemporaryAbsent,
   getTemporaryAbsentList,
   createTemporaryAbsent,
   getTemporaryAbsentById,
   getTemporaryAbsentByResidentId,
   deleteTemporaryAbsent,
};
