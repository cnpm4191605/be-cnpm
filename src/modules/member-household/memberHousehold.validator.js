import Joi from "joi";

const createMemberHousehold = {
   body: Joi.object({
      idNhanKhau: Joi.number().positive().required(),
      idHoKhau: Joi.number().positive().required(),
      quanHeVoiChuHo: Joi.string().trim().required(),
   }),
};
const updateMemberHouseholdById = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
   body: Joi.object({
      idNhanKhau: Joi.number().positive().required(),
      idHoKhau: Joi.number().positive().required(),
      quanHeVoiChuHo: Joi.string().trim().required(),
   }),
};

const getMemberHouseholdById = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const deleteMemberHousehold = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const getMemberHouseholdByHouseholdId = {
   params: Joi.object({
      householdId: Joi.number().positive().required(),
   }),
};
export {
   createMemberHousehold,
   getMemberHouseholdById,
   updateMemberHouseholdById,
   deleteMemberHousehold,
   getMemberHouseholdByHouseholdId,
};
