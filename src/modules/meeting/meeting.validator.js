import Joi from "joi";

const createMeeting = {
   body: Joi.object({
      title: Joi.string().trim().required(),
      location: Joi.string().trim().required(),
      startTime: Joi.string().trim().required(""),
      notes: Joi.string().trim().allow("").optional(),
   }),
};
const getMeetingById = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const getMeetingByHouseholdId = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
const deleteMeeting = {
   params: Joi.object({
      id: Joi.number().positive().required(),
   }),
};
export {
   createMeeting,
   getMeetingById,
   getMeetingByHouseholdId,
   deleteMeeting,
};
