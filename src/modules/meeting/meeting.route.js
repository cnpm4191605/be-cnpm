import { Router } from "express";
import { HttpStatus, UserRole } from "../../constant.js";
import validate from "../../middlewares/validate.js";
import * as meetingValidator from "./meeting.validator.js";
import * as meetingController from "./meeting.controller.js";
import { verifyToken, checkUserRole } from "../../middlewares/authJwt.js";
const router = Router();
router.get(
   "/household/:id",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(meetingValidator.getMeetingByHouseholdId),
   meetingController.getMeetingByHouseholdId
);
router.get(
   "/:id",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(meetingValidator.getMeetingById),
   meetingController.getMeetingById
);

router.get(
   "/",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],

   meetingController.getMeetingList
);
router.post(
   "/",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(meetingValidator.createMeeting),
   meetingController.createMeeting
);
router.delete(
   "/:id",
   [
      verifyToken,
      checkUserRole([
         UserRole.PRESIDENT,
         UserRole.VICE_PRESIDENT,
         UserRole.SECRETARY,
      ]),
   ],
   validate(meetingValidator.deleteMeeting),
   meetingController.deleteMeeting
);
export default router;
