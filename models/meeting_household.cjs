"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class Meeting_Household extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         Meeting_Household.belongsTo(models.Meeting, {
            foreignKey: "meetingId",
            targetKey: "id",
         });
         Meeting_Household.belongsTo(models.Household, {
            foreignKey: "householdId",
            targetKey: "id",
         });
      }
   }
   Meeting_Household.init(
      {
         meetingId: { type: DataTypes.INTEGER },
         householdId: { type: DataTypes.INTEGER },
      },
      {
         sequelize,
         freezeTableName: true,
         paranoid: true,
      }
   );
   return Meeting_Household;
};
