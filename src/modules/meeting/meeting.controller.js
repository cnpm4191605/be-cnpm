import { HttpStatus, UserRole } from "../../constant.js";
import ErrorResponse from "../../utils/ErrorResponse.js";
import SuccessResponse from "../../utils/SuccessResponse.js";
import * as meetingService from "./services/meeting.service.js";

const getMeetingList = async (req, res) => {
   try {
      const meetingList = await meetingService.getMeetingList();
      return res.status(HttpStatus.OK).json(new SuccessResponse(meetingList));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const createMeeting = async (req, res) => {
   try {
      const newMeeting = await meetingService.createMeeting(req.body);
      // if (newMeeting) {
      //    let meeting = await meetingService.getMeetingById(newMeeting.id);
      return res.status(HttpStatus.OK).json(new SuccessResponse(newMeeting));
      // }
      // return res
      //    .status(HttpStatus.BAD_REQUEST)
      //    .json(new ErrorResponse(HttpStatus.BAD_REQUEST, "create failed"));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const getMeetingById = async (req, res) => {
   try {
      const meeting = await meetingService.getMeetingById(req.params.id);
      return res.status(HttpStatus.OK).json(new SuccessResponse(meeting));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const getMeetingByHouseholdId = async (req, res) => {
   try {
      // lay danh sach id theo householdId tu model Meeting_Household
      let listId = [];
      const meetingList = await meetingService.getMeetingByListId(listId);
      return res.status(HttpStatus.OK).json(new SuccessResponse(meetingList));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const deleteMeeting = async (req, res) => {
   try {
      const isDeleted = await meetingService.getMeetingById(req.params.id);
      if (isDeleted) {
         return res
            .status(HttpStatus.OK)
            .json(new SuccessResponse(req.params.id));
      }
      return res
         .status(HttpStatus.BAD_REQUEST)
         .json(new ErrorResponse(HttpStatus.BAD_REQUEST, "delete failed"));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
export {
   getMeetingList,
   createMeeting,
   getMeetingById,
   getMeetingByHouseholdId,
   deleteMeeting,
};
