/**
 * Author: Vũ Đình Tiến
 * Date : 5/02/2023
 * Project : Quản lý khu dân cư
 * Teacher: Trịnh Thành Trung
 * Class name: Nhập môn công nghệ phần mềm
 * Class code  : 136813
 * Description: các hàm crud khai báo tạm vắng với database bằng ORM
 */

import { Op } from "sequelize";
import db from "../../../../models/index.cjs";
let getTemporaryAbsentList = async () => {
   try {
      return await db.TemporaryAbsent.findAll({
         include: [
            {
               model: db.Resident,
               attributes: ["soCMT", "hoTen"],
            },
         ],
      });
   } catch (error) {
      throw error;
   }
};
let getTemporaryAbsentListNow = async () => {
   try {
      return await db.TemporaryAbsent.findAll({
         where: {
            tuNgay: {
               [Op.lte]: new Date(),
            },
            denNgay: {
               [Op.gte]: new Date(),
            },
         },
      });
   } catch (error) {
      throw error;
   }
};
let getTemporaryAbsentById = async (id) => {
   try {
      return await db.TemporaryAbsent.findOne({
         where: {
            id,
         },
         include: [
            {
               model: db.Resident,
               attributes: ["soCMT", "hoTen"],
            },
         ],
      });
   } catch (error) {
      throw error;
   }
};
let getTemporaryAbsentByResidenceId = async (residentId) => {
   try {
      return await db.TemporaryAbsent.findAll({
         where: { idNhanKhau: residentId },
         include: [
            {
               model: db.Resident,
               attributes: ["soCMT", "hoTen"],
            },
         ],
      });
   } catch (error) {
      throw error;
   }
};
let createTemporaryAbsent = async (createBody) => {
   try {
      return await db.TemporaryAbsent.create({ ...createBody });
   } catch (error) {
      throw error;
   }
};
let updateTemporaryAbsent = async (id, updateBody) => {
   try {
      return await db.TemporaryAbsent.update(
         {
            ...updateBody,
         },
         {
            where: { id },
         }
      );
   } catch (error) {
      throw error;
   }
};
let deleteTemporaryAbsent = async (id) => {
   try {
      return await db.TemporaryAbsent.destroy({ where: id });
   } catch (error) {
      throw error;
   }
};
let countTemporaryAbsent = async () => {
   try {
      return await db.TemporaryAbsent.count({
         where: {
            tuNgay: {
               [Op.lte]: new Date(),
            },
            denNgay: {
               [Op.gte]: new Date(),
            },
         },
      });
   } catch (error) {
      throw error;
   }
};
export {
   countTemporaryAbsent,
   getTemporaryAbsentList,
   getTemporaryAbsentById,
   getTemporaryAbsentByResidenceId,
   createTemporaryAbsent,
   updateTemporaryAbsent,
   deleteTemporaryAbsent,
   getTemporaryAbsentListNow,
};
