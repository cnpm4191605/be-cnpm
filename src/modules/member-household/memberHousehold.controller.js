import { HttpStatus, UserRole } from "../../constant.js";
import ErrorResponse from "../../utils/ErrorResponse.js";
import SuccessResponse from "../../utils/SuccessResponse.js";
import * as memberHouseholdService from "./services/memberHousehold.service.js";

const createMemberHousehold = async (req, res) => {
   try {
      const memberHousehold =
         await memberHouseholdService.createMemberHousehold(req.body);
      return res
         .status(HttpStatus.OK)
         .json(new SuccessResponse(memberHousehold));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const getMemberHouseholdById = async (req, res) => {
   try {
      const memberHousehold =
         await memberHouseholdService.getMemberHouseholdById(req.params.id);
      return res
         .status(HttpStatus.OK)
         .json(new SuccessResponse(memberHousehold));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const getMemberHouseholdByHouseholdId = async (req, res) => {
   try {
      const members =
         await memberHouseholdService.getMemberHouseholdByHouseholdId(
            req.params.householdId
         );
      return res.status(HttpStatus.OK).json(new SuccessResponse(members));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const updateMemberHouseholdById = async (req, res) => {
   try {
      const isUpdated = await memberHouseholdService.updateMemberHousehold(
         req.params.id,
         req.body
      );
      if (isUpdated) {
         return res.status(HttpStatus.OK).json(new SuccessResponse("OKE"));
      }
      return res
         .status(HttpStatus.BAD_REQUEST)
         .json(new ErrorResponse(HttpStatus.BAD_REQUEST, "update failed"));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const deleteMemberHousehold = async (req, res) => {
   try {
      const isDeleted = await memberHouseholdService.deleteMemberHousehold(
         req.params.id
      );
      if (isDeleted) {
         return res
            .status(HttpStatus.OK)
            .json(new SuccessResponse(req.params.id));
      }
      return res
         .status(HttpStatus.BAD_REQUEST)
         .json(new ErrorResponse(HttpStatus.BAD_REQUEST, "delete failed"));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
const deleteMemberHouseholdByResidentId = async (req, res) => {
   try {
      await memberHouseholdService.deleteMemberHouseholdByResidentId(
         req.params.id
      );
      return res.status(HttpStatus.OK).json(new SuccessResponse("OKE"));
   } catch (error) {
      return res
         .status(HttpStatus.INTERNAL_SERVER_ERROR)
         .json(
            new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, error.message)
         );
   }
};
export {
   createMemberHousehold,
   getMemberHouseholdById,
   updateMemberHouseholdById,
   deleteMemberHousehold,
   getMemberHouseholdByHouseholdId,
   deleteMemberHouseholdByResidentId,
};
