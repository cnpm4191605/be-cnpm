import Sequelize from "sequelize";
const Op = Sequelize.Op;
import db from "../../../../models/index.cjs";
let getMeetingHouseholdList = async () => {
   try {
      return await db.Meeting_Household.findAll();
   } catch (error) {
      throw error;
   }
};
let getMeetingHouseholdById = async (id) => {
   try {
      return await db.Meeting_Household.findOne({
         where: {
            id,
         },
      });
   } catch (error) {
      throw error;
   }
};
let getMeetingHouseholdByHouseholdId = async (householdId) => {
   try {
      return await db.Meeting_Household.findAll({
         where: {
            householdId,
         },
      });
   } catch (error) {
      throw error;
   }
};
let getMeetingHouseholdByMeetingId = async (meetingId) => {
   try {
      return await db.Meeting_Household.findAll({
         where: {
            meetingId: meetingId,
         },
      });
   } catch (error) {
      throw error;
   }
};
let createMeetingHousehold = async (createBody) => {
   try {
      return await db.Meeting_Household.create({ ...createBody });
   } catch (error) {
      throw error;
   }
};
let updateMeetingHousehold = async (id, updateBody) => {
   try {
      return await db.Meeting_Household.update(
         {
            ...updateBody,
         },
         {
            where: { id },
         }
      );
   } catch (error) {
      throw error;
   }
};
let deleteMeetingHousehold = async (id) => {
   try {
      return await db.Meeting_Household.destroy({ where: id });
   } catch (error) {
      throw error;
   }
};
export {
   getMeetingHouseholdList,
   getMeetingHouseholdById,
   getMeetingHouseholdByHouseholdId,
   getMeetingHouseholdByMeetingId,
   createMeetingHousehold,
   updateMeetingHousehold,
   deleteMeetingHousehold,
};
